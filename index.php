<?php

require_once(__DIR__."/vendor/autoload.php");

use Mardock\Openpay\Openpay;

Openpay::init('mkrlbgo9ndyr8jvyeb8j','sk_d0cdc87b6d9f4044a7e73a55baaee62c'); //siempre se debe ejecutar este metodo antes de realizar cualquier operacion. Como primer parametro esta el id (openpay) y la llave privada.
Openpay::setProductionMode(false); //para pasar a produccion pasamos true, para sandbox es false. Si estamos en sandbox no es necesario ejecutar este metodo, por default estamos en sandbox
Openpay::getProductionMode(); //obtenemos true si estamos en produccion y false si estamos en sandbox

// //Available methods

// $data['name'] = 'NOMBRE(S)';
// $data['last_name'] = 'APELLIDO(S)';
// $data['email'] = 'correo@correo.com';
// $data['phone_number'] = '1234567890';
// Openpay::addCustomer($params); //damos de alta un cliente, recibe un array con los indices name,last_name,email,phone_number.

// Openpay::getCustomer($customer_id); //obtenemos la informacion de un cliente, recibe como parametro el id del cliente, este se obtiene al crearlo.

// $data['card_number'] = '1234657980123';
// $data['holder_name'] = 'NOMBRE DEL PROPIETARIO DE LA TARJETA';
// $data['expiration_year'] = '24';
// $data['expiration_month'] = '02';
// $data['cvv2'] = '123';
// Openpay::addCard($customer_id, $params); //damos de alta un nuevo metodo de pago (tarjeta), recibe como parametro el id del cliente.

// Openpay::getCards($customer_id); //obtenemos las tarjetas dadas de alta de un cliente, recibe como parametro el id del cliente.

// Openpay::deleteCard($customer_id, $card_id); //eliminamos un metodo de pago (tarjeta), recibe el id del cliente y el id de la tarjeta (se obtiene al crearla)

// $data['source_id'] = 'AAAAAAAAAAAAAAAAAA';
// $data['amount'] = '50';
// $data['description'] = 'descripcion del pago';
// $data['device_session_id'] = 'TOKENJAVASCRIPT';
// $data['currency'] = 'MXN';
// $data['redirect_url'] = 'dulcevida.com/redirect/';
// $data['use_3d_secure'] = true;
// Openpay::addCharges($customer_id, $params); //realizamos un cargo a un cliente, como parametro el id del cliente.

//echo json_encode(Openpay::getCharges('ahrqufywpfguhov03o6c'));
//echo json_encode(Openpay::getCustomer('ahrqufywpfguhov03o6c'));
//echo json_encode(Openpay::getCards('ahrqufywpfguhov03o6c'));

// $data['amount'] = 500;
// $data['description'] = 'Pago de Testing';
// $data['send_email'] = false;
// $data['redirect_url'] = 'lockeypm.com/contact';
// $data['currency'] = 'MXN';

 echo json_encode(Openpay::addChargesWithRedirection('ahrqufywpfguhov03o6c' , $datos));

?>