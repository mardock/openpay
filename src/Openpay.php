<?php

namespace Mardock\Openpay;

use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use Mardock\Openpay\Shortener;
use GuzzleHttp\Exception\GuzzleException;

class Openpay
{
    use Shortener, Toolbox;

    private static $client;

    private static $apiProduction = 'https://api.openpay.mx';
    private static $apiSandbox = 'https://sandbox-api.openpay.mx';
    private static $apiVersion = '/v1/';

    private static $apiBase = null;

    private static $id = '';
    private static $privateKey = '';
    private static $country = 'MX';

    private static $sandboxMode = true;

    public function __construct()
    {
        
    }
    public static function init($id, $privateKey){
        self::$id = $id;
        self::$privateKey = $privateKey;

        $apiBase = self::$sandboxMode == false ? self::$apiProduction : self::$apiSandbox;
        $apiBase = $apiBase.self::$apiVersion.self::$id;
        
        self::$apiBase = $apiBase;

        Connection::init();
    }

    public static function setId($id){
        self::$id = $id;
    }
    public static function getId(){
        return self::$id;
    }

    public static function setPrivateKey($privateKey){
        return self::$privateKey = $privateKey;
    }
    public static function getPrivateKey(){
        return self::$privateKey;
    }

    public static function setProductionMode(bool $sandboxMode){
        self::$sandboxMode = !$sandboxMode;
        self::init(self::$id,self::$privateKey);
    }
    public static function getProductionMode(){
        return self::$sandboxMode == true ? false : true;
    }
    
    public static function getApiBase(){
        return self::$apiBase;
    }

    public static function getCustomers()
    {
        $response = Connection::getClient()->request('GET','customers');

        $response = json_decode($response->getBody());

        return $response;
    }

    public static function getCustomer($customer_id)
    {
        $response = Connection::getClient()->request('GET','customers/'.$customer_id);

        $response = json_decode($response->getBody());

        return $response;
    }

    public static function addCustomer($params = [])
    {
        try{
            $params = [
                'name' => $params['name'],
                'last_name' => $params['last_name'],
                'email' => $params['email'],
                'requires_account' => false,
                'phone_number' => $params['phone'],
            ];
            
            $response = Connection::getClient()->request('POST','customers',[
                RequestOptions::JSON => $params
            ]);

            $response = json_decode($response->getBody());

            return $response;

        }catch (GuzzleException $e) {
            
            $response = json_decode($e->getResponse()->getBody());

            return $response;
            
        }
    }

    public static function getCards($customer_id)
    {
        try{
            $response = Connection::getClient()->request('GET','customers/'.$customer_id.'/cards');

            $response = json_decode($response->getBody());

            return $response;

        }catch (GuzzleException $e) {
            
            $response = json_decode($e->getResponse()->getBody());

            return $response;
            
        }
    }

    public static function addCard($customer_id, $params = [])
    {
        try{
            $params = [
                "card_number" => $params['card_number'],
                "holder_name" => $params['holder_name'],
                "expiration_year" => $params['expiration_year'],
                "expiration_month" => $params['expiration_month'],
                "cvv2" => $params['cvv2'],
            ];

            $response = Connection::getClient()->request('POST','customers/'.$customer_id.'/cards',[
                RequestOptions::JSON => $params
            ]);

            $response = json_decode($response->getBody());

            $data = [];
            $data['card_number'] = $response->card_number;
            $data['name'] = $response->holder_name;
            $data['expiration_date'] = $response->expiration_month.'/'.$response->expiration_year;
            $data['type'] = $response->type;
            $data['bank'] = $response->bank_name;

            return $response;
            
        }catch (GuzzleException $e) {
            
            $response = json_decode($e->getResponse()->getBody());
            
            self::convertMessageError($response->description,$response->error_code);

            return $response;

        }catch(Exception $e){
            return $e;
        }
    }
    public static function deleteCard($customer_id, $card_id)
    {
        try{
            $response = Connection::getClient()->request('DELETE','customers/'.$customer_id.'/cards/'.$card_id);

            $response = json_decode($response->getBody());
            
            $response = empty($response)? ['message' => 'Successful']: $response;

            return $response;

        }catch (GuzzleException $e) {
            
            $response = json_decode($e->getResponse()->getBody());

            return $response;
            
        }
    }
   
    public static function getCharges($customer_id)
    {
        try{
            $response = Connection::getClient()->request('GET','customers/'.$customer_id.'/charges');

            $response = json_decode($response->getBody());

            return $response;

        }catch (GuzzleException $e) {
            
            $response = json_decode($e->getResponse()->getBody());

            return $response;
            
        }
    }
    public static function getCharge($customer_id = null, $charge_id)
    {
        try{
            if($customer_id != null){
                $response = Connection::getClient()->request('GET','customers/'.$customer_id.'/charges/'.$charge_id);
            }else{
                $response = Connection::getClient()->request('GET','charges/'.$charge_id);
            }

            $response = json_decode($response->getBody());

            if($response->error_message){
                self::convertMessageError($response->error_message);
            }
            
            return $response;

        }catch (GuzzleException $e) {
            
            $response = json_decode($e->getResponse()->getBody());

            return $response;
            
        }
    }

    /*hacer un cargo a un cliente */
    public static function addCharges($customer_id = null, $params = [])
    {        
        try{
            $data = [
                "method" => 'card',
                "source_id" => $params['source_id'],
                "amount" => $params['total'],
                "description" => $params['description'],
                'device_session_id' => $params['device_session_id'],
                'currency'  =>  $params['currency']??'USD',
                'redirect_url' => $params['redirect_url'],
                'use_3d_secure' => $params['use_3d_secure']??true,
            ];

            if($customer_id == null){
                $data['customer'] = [
                    'name' => $params['customer']['name'],
                    'last_name' => $params['customer']['last_name'],
                    'phone_number' => $params['customer']['phone_number'],
                    'email' => $params['customer']['email']
                ];
                
                $response = Connection::getClient()->request('POST','charges',[
                    RequestOptions::JSON => $data
                ]);
    
                $response = json_decode($response->getBody());
    
                return $response;    
            }

            $response = Connection::getClient()->request('POST','customers/'.$customer_id.'/charges',[
                RequestOptions::JSON => $data
            ]);

            $response = json_decode($response->getBody());

            return $response;

        }catch (GuzzleException $e) {
            
            $response = json_decode($e->getResponse()->getBody());

            return $response;
            
        }
    }
    public static function addChargesWithRedirection($customer_id = null, $params = [])
    {        
        try{
            $params = [
                'method' => 'card',
                'amount' => $params['amount'],
                'description' => $params['description'],
                'confirm' => false,
                'send_email' => $params['send_email'],
                'redirect_url' => $params['redirect_url'],
                'currency'  =>  $params['currency']??'USD',
            ];

            $customer_id == null ? array_push($params, [
                'customer'  =>  [
                    'name' => $params['customer']['name'],
                    'last_name' => $params['customer']['last_name'],
                    'phone_number' => $params['customer']['phone_number'],
                    'email' => $params['customer']['email'],
                ]
            ]) : null;
            
            $uri = $customer_id == null ? 'charges' : 'customers/'.$customer_id.'/charges';

            $response = Connection::getClient()->request('POST', $uri ,[
                RequestOptions::JSON => $params
            ]);

            $response = json_decode($response->getBody());

            return $response;

        }catch (GuzzleException $e) {
            
            $response = json_decode($e->getResponse()->getBody());

            return $response;
            
        }
    }

    /*Funciones para pasar las pruebas de openpay */

    /*funcio para crear un token en openpay*/
    public static function tokenization(){
        try{
            $params = [
                // "holder_name" => $request->holder_name,
                // "card_number" => $request->card_number,
                // "cvv2" => $request->cvv2,
                // "expiration_month" => $request->expiration_month,
                // "expiration_year" => $request->expiration_year,
            ];

            $response = Connection::getClient()->request('POST','tokens',[
                RequestOptions::JSON => $params
            ]);

            $response = json_decode($response->getBody());

            return $response;
        }catch (GuzzleException $e) {
            
            $response = json_decode($e->getResponse()->getBody());

            return $response;
            
        }
    }

   
    /* Planes */
    public static function addPlan($params = [])
    {        
        try{
            $params = [
                'name' => $params['name'],
                'amount' => $params['amount'],
                'repeat_every' => $params['repeat_every'],
                'repeat_unit' => $params['repeat_unit'],
                'retry_times' => $params['retry_times'],
                'status_after_retry' => $params['status_after_retry'],
                'trial_days'  =>  $params['trial_days'],
            ];
            
            $response = Connection::getClient()->request('POST', 'plans' ,[
                RequestOptions::JSON => $params
            ]);

            $response = json_decode($response->getBody());

            return $response;

        }catch (GuzzleException $e) {
            
            $response = json_decode($e->getResponse()->getBody());

            return $response;
            
        }
    }
    public static function updatePlan($plan_id ,$params = [])
    {        
        try{
            $params = [
                'name' => $params['name'],
                'trial_days'  =>  $params['trial_days'],
            ];
            
            $response = Connection::getClient()->request('PUT', 'plans/'.$plan_id ,[
                RequestOptions::JSON => $params
            ]);

            $response = json_decode($response->getBody());

            return $response;

        }catch (GuzzleException $e) {
            
            $response = json_decode($e->getResponse()->getBody());

            return $response;
            
        }
    }
    public static function getPlan($plan_id)
    {        
        try{
            
            $response = Connection::getClient()->request('GET', 'plans/'.$plan_id ,[]);

            $response = json_decode($response->getBody());

            return $response;

        }catch (GuzzleException $e) {
            
            $response = json_decode($e->getResponse()->getBody());

            return $response;
            
        }
    }
    public static function getPlans()
    {        
        try{
            
            $response = Connection::getClient()->request('GET', 'plans' ,[]);

            $response = json_decode($response->getBody());

            return $response;

        }catch (GuzzleException $e) {
            
            $response = json_decode($e->getResponse()->getBody());

            return $response;
            
        }
    }
    /* Subscripciones */
    public static function addSubscription($customer_id ,$params = [])
    {        
        try{
            $params = [
                'plan_id' => $params['plan_id'],
                'trial_end_date'  =>  $params['trial_end_date'],
                'source_id'  =>  $params['source_id'],
            ];

            array_key_exists('source_id', $params) ? array_push($params, [
                'card'  =>  [
                    'card_number' => $params['card']['card_number'],
                    'holder_name' => $params['card']['holder_name'],
                    'expiration_year' => $params['card']['expiration_year'],
                    'expiration_month' => $params['card']['expiration_month'],
                    'cvv2' => $params['card']['cvv2'],
                    'device_session_id' => $params['card']['device_session_id'],
                ]
            ]) : null;
            
            $response = Connection::getClient()->request('POST', 'customers/'.$customer_id.'/subscriptions' ,[
                RequestOptions::JSON => $params
            ]);

            $response = json_decode($response->getBody());

            return $response;

        }catch (GuzzleException $e) {
            
            $response = json_decode($e->getResponse()->getBody());

            return $response;
            
        }
    }
    public static function updateSubscription($customer_id , $subscription_id ,$params = [])
    {        
        try{
            $params = [
                'cancel_at_period_end' => $params['plan_id'],
                'trial_end_date'  =>  $params['trial_end_date'],
                'source_id'  =>  $params['source_id'],
            ];

            array_key_exists('source_id', $params) ? array_push($params, [
                'card'  =>  [
                    'card_number' => $params['card']['card_number'],
                    'holder_name' => $params['card']['holder_name'],
                    'expiration_year' => $params['card']['expiration_year'],
                    'expiration_month' => $params['card']['expiration_month'],
                    'cvv2' => $params['card']['cvv2'],
                    'device_session_id' => $params['card']['device_session_id'],
                ]
            ]) : null;
            
            $response = Connection::getClient()->request('PUT', 'customers/'.$customer_id.'/subscriptions/'.$subscription_id ,[
                RequestOptions::JSON => $params
            ]);

            $response = json_decode($response->getBody());

            return $response;

        }catch (GuzzleException $e) {
            
            $response = json_decode($e->getResponse()->getBody());

            return $response;
            
        }
    }
    public static function getSubscriptions($customer_id)
    {        
        try{
            
            $response = Connection::getClient()->request('GET', 'customers/'.$customer_id.'/subscriptions');

            $response = json_decode($response->getBody());

            return $response;

        }catch (GuzzleException $e) {
            
            $response = json_decode($e->getResponse()->getBody());

            return $response;
            
        }
    }
    public static function getSubscription($customer_id , $subscription_id)
    {        
        try{
            
            $response = Connection::getClient()->request('GET', 'customers/'.$customer_id.'/subscriptions/'.$subscription_id);

            $response = json_decode($response->getBody());

            return $response;

        }catch (GuzzleException $e) {
            
            $response = json_decode($e->getResponse()->getBody());

            return $response;
            
        }
    }
    public static function deleteSubscription($customer_id , $subscription_id)
    {        
        try{
            
            $response = Connection::getClient()->request('DELETE', 'customers/'.$customer_id.'/subscriptions/'.$subscription_id);

            $response = json_decode($response->getBody());

            return $response;

        }catch (GuzzleException $e) {
            
            $response = json_decode($e->getResponse()->getBody());

            return $response;
            
        }
    }

    public static function addChargesSPEICommerce($amount, $description)
    {
        try{
            $params = [
                'method' => 'bank_account',
                'amount'  =>  $amount,
                'description'  =>  $description,
            ];

            $response = Connection::getClient()->request('POST', 'charges',[
                RequestOptions::JSON => $params
            ]);

            $response = json_decode($response->getBody());

            return $response;

        }catch (GuzzleException $e) {
            
            $response = json_decode($e->getResponse()->getBody());

            return $response;
            
        }
    }

    public static function addChargesSPEIClient($customer_id, $amount, $description)
    {
        try{
            $params = [
                'method' => 'bank_account',
                'amount'  =>  $amount,
                'description'  =>  $description,
            ];

            $response = Connection::getClient()->request('POST', 'customers/'.$customer_id.'/charges',[
                RequestOptions::JSON => $params
            ]);

            $response = json_decode($response->getBody());

            return $response;

        }catch (GuzzleException $e) {
            
            $response = json_decode($e->getResponse()->getBody());

            return $response;
            
        }
    }
    public static function getChargesSPEIClient($customer_id, $transaction_id)
    {
        try{

            $response = Connection::getClient()->request('GET', 'customers/'.$customer_id.'/transfers/'.$transaction_id);

            $response = json_decode($response->getBody());

            return $response;

        }catch (GuzzleException $e) {
            
            $response = json_decode($e->getResponse()->getBody());

            return $response;
            
        }
    }
    public static function addCardToken($params)
    {
        try{
            $params = [
                "card_number" => $params['card_number'],
                "holder_name" => $params['holder_name'],
                "expiration_year" => $params['expiration_year'],
                "expiration_month" => $params['expiration_month'],
                "cvv2" => $params['cvv2'],
            ];
            
            array_key_exists('address',$params) ? array_push($params, [
                "address" => [
                    "city"=> $params['city'],
                    "country_code"=> $params['country_code'],
                    "postal_code"=> $params['postal_code'],
                    "line1"=> $params['line1'],
                    "line2"=> $params['line2'],
                    "line3"=> $params['line3'],
                    "state"=> $params['state'],
                ]
            ]) : null;

            $response = Connection::getClient()->request('POST', 'tokens',[
                RequestOptions::JSON => $params
            ]);

            $response = json_decode($response->getBody());

            if($response->error_code){
                self::convertMessageError($response->description);
            }

            return $response;

        }catch (GuzzleException $e) {
            
            $response = json_decode($e->getResponse()->getBody());

            return $response;
            
        }
    }
    public static function addCardTokenCustomer($customer_id , $params)
    {
        try{
            $params = [
                "token_id" => $params['token_id'],
                "device_session_id" => $params['device_session_id'],
            ];

            $response = Connection::getClient()->request('POST', 'customers/'.$customer_id.'/cards',[
                RequestOptions::JSON => $params
            ]);

            $response = json_decode($response->getBody());

            return $response;

        }catch (GuzzleException $e) {
            
            $response = json_decode($e->getResponse()->getBody());

            return $response;
            
        }
    }
}