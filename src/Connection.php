<?php

namespace Mardock\Openpay;

use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use GuzzleHttp\Exception\GuzzleException;

class Connection
{
    private static $client;

    public function __construct()
    {

    }

    public static function init(){
        self::$client = new Client([
            'base_uri' => Openpay::getApiBase().'/',
            'auth' => [ Openpay::getPrivateKey() , null ],
            'header'    =>  [ 'Content-Type'    =>  'application/json'],
        ]);
    }
    public static function getClient(){
        return self::$client;
    }
}