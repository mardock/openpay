<?php

namespace Mardock\Openpay;

use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use GuzzleHttp\Exception\GuzzleException;

trait Shortener
{
    public static function short($url,$length = 5)
    {
        return random_bytes($length);
    }
}