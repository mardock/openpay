<?php

namespace Mardock\Openpay;

trait Toolbox
{
    public static function convertMessageError(&$message, $code = null)
    {
        $messages = [
            'The card number verification digit is invalid' => 'El número de tarjeta es invalido.',
            'The expiration date has expired' => 'La fecha de expiración de la tarjeta es anterior a la fecha actual.',
            'The CVV2 security code is required' => 'El código de seguridad de la tarjeta (CVV2) no fue proporcionado.',
            'The card number is only valid in sandbox' => 'El número de tarjeta es de prueba, solamente puede usarse en Sandbox.',
            'The card is not valid for points' => 'La tarjeta no es valida para pago con puntos.',
            'The CVV2 security code is invalid' => 'El código de seguridad de la tarjeta (CVV2) es inválido.',
            '3D Secure authentication failed' => 'Autenticación  3D Secure fallida.',
            'Card product type not supported' => 'Tipo de tarjeta no soportada.',
            'The card was declined by the bank' => 'La tarjeta fue declinada por el banco.',
            'The card has expired' => 'La tarjeta ha expirado.',
            "The card doesn't have sufficient funds" => 'La tarjeta no tiene fondos suficientes.',
            'The card was reported as stolen' => 'La tarjeta ha sido identificada como una tarjeta robada.',
            'Fraud risk detected by anti-fraud system --- Found in blacklist' => 'La tarjeta ha sido rechazada por el sistema antifraude.',
            'Request not allowed' => 'La operación no esta permitida para este cliente o esta transacción.',
            'The card was reported as lost' => 'La tarjeta fue reportada como perdida.',
            'The bank has restricted the card' => 'El banco ha restringido la tarjeta.',
            'The bank has requested the card to be retained' => 'El banco ha solicitado que la tarjeta sea retenida. Contacte al banco.',
            'Bank authorization is required for this charge' => 'Se requiere solicitar al banco autorización para realizar este pago.',
            'Merchant not authorized to use payment plan' => 'Comercio no autorizado para procesar pago a meses sin intereses.',
            'Invalid promotion for such card type' => 'Promoción no valida para este tipo de tarjetas.',
            'Transaction amount is less than minimum for promotion' => 'El monto de la transacción es menor al mínimo permitido para la promoción.',
            'Promotion not allowed' => 'Promoción no permitida.',
            'cvv2 length must be 3 digits' => 'La longitud del cvv2 debe ser de 3 dígitos',
            'cvv2 length must be 4 digits' => 'La longitud del cvv2 debe ser de 4 dígitos',
            'card_number must contain only digits' => 'El número de tarjeta debe contener sólo dígitos'
        ];
        $codes = [
            '2004' => 'El número de tarjeta es invalido.',
            '2005' => 'La fecha de expiración de la tarjeta es anterior a la fecha actual.',
            '2006' => 'El código de seguridad de la tarjeta (CVV2) no fue proporcionado.',
            '2007' => 'El número de tarjeta es de prueba, solamente puede usarse en Sandbox.',
            '2008' => 'La tarjeta no es valida para pago con puntos.',
            '2009' => 'El código de seguridad de la tarjeta (CVV2) es inválido.',
            '2010' => 'Autenticación 3D Secure fallida.',
            '2011' => 'Tipo de tarjeta no soportada.',
            '3001' => 'La tarjeta fue declinada por el banco.',
            '3002' => 'La tarjeta ha expirado.',
            '3003' => 'La tarjeta no tiene fondos suficientes.',
            '3004' => 'La tarjeta ha sido identificada como una tarjeta robada.',
            '3005' => 'La tarjeta ha sido rechazada por el sistema antifraude.',
            '3006' => 'La operación no esta permitida para este cliente o esta transacción.',
            '3009' => 'La tarjeta fue reportada como perdida.',
            '3010' => 'El banco ha restringido la tarjeta.',
            '3011' => 'El banco ha solicitado que la tarjeta sea retenida. Contacte al banco.',
            '3012' => 'Se requiere solicitar al banco autorización para realizar este pago.',
            '3201' => 'Comercio no autorizado para procesar pago a meses sin intereses.',
            '3203' => 'Promoción no valida para este tipo de tarjetas.',
            '3204' => 'El monto de la transacción es menor al mínimo permitido para la promoción.',
            '3205' => 'Promoción no permitida.',
        ];      

        $message = array_key_exists($code,$codes) ? $codes[$code] : $messages[$message];

        return $message;
    }
}